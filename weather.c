#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>

const int PORT = 80;
const char HOST[] = "api.wunderground.com";

char PATH[60];

const char PATH1[40] = "/api/1655f919bbcd29ed/conditions/q/";
const char PATH3[6] = ".json";

//NOTE: This program usees hard-wired values for PORT
//HOST and PATH. For a program that needs to request 
// A document depending on what the user needs, you'll
//want to put them in variables
char PATH2[10] = "";
int main(int argc,char**argv)
{
    strcpy(PATH2, argv[1]);
    
    strcpy(PATH, PATH1);
    strcat(PATH, PATH2);
    strcat(PATH, PATH3);
   // printf("%s\n", PATH);
    
    struct sockaddr_in sa;
    int sockfd;
    
    printf("Starting\n");
    
    printf("Looking Up IP Address for %s\n",HOST);
    struct hostent *he = gethostbyname(HOST);
    
    struct in_addr *ip = (struct in_addr *)he->h_addr_list[0];
    
    printf("IP address is %s\n",inet_ntoa(*ip));
    
    printf("Creating Structures\n");
    sa.sin_family = AF_INET;
    sa.sin_port = htons(PORT); // Port 80 for http
    sa.sin_addr = *((struct in_addr *)ip);
    
    printf("Creating Socket\n");
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    if(sockfd == -1)
    {
        fprintf(stderr,"Cant Create Socket\n");
        exit(3);
    }
    
    printf("Connecting\n");
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if(res == -1)
    {
        fprintf(stderr, "Cant connect\n");
        exit(2);
    }
    
    //once connected, convert the sockfd into a FILE *
    // that we can read or write to
    FILE *sock = fdopen(sockfd, "r+");
   // FILE *sock2 = fdopen(sockfd, "r+");
    
    //Now we can use fprintf, fgets, fscanf, ect to ineract with the
    //server
    printf("Sending GET\n");
    fprintf(sock, "GET %s HTTP/1.0\n", PATH); //PATH being requested
    fprintf(sock, "HOST: %s\n", HOST);
    fprintf(sock, "\n");
    
    printf("Receiving response\n");
    char buf[1000];
    
    char line0[100];
    char line1[100];
    char line2[100];
    char line3[100];
    char line4[100];
    char line5[100];
    char line6[100];
    
    int c1 = 0; //on off for observation
    int c2 = 0;
    int c3 = 0;
    int c4 = 0;
    int c5 = 0;
    int c6 = 0;
    
    while((fgets(buf, 1000, sock)) != NULL)
    { 
        
        //printf("%s\n", buf);
        if ((strstr(buf,"observation_time")) && c1 == 0)
        {
            sprintf(line0,"%s","Current Conditions");
            
            int C = 0;
            for(int i = 0; i < 100; i++)
            {
                if( (buf[i] == ',') || buf[i] == 0x22)
                {
                    if(buf[i] == ',')
                    {
                        if(C != 0)
                        {
                            buf[i] = 0x20;
                            C = 0;
                        }
                        C++;
                    }
                    if(buf[i] == 0x22)
                    {
                        buf[i] = 0x20;
                    }
                }
            }
            sprintf(line1,"Observation time: %s",&buf[22]);
            c1 = 1;
            C = 0;
        }
        if ((strstr(buf,"full")) && c2 == 0)
        {
            int commacount = 0; 
            
            for(int i = 0; i < 100; i++)
            {
                if( (buf[i] == ',') || buf[i] == 0x22)
                {
                    if(buf[i] == ',')
                    {
                        if(commacount != 0)
                        {
                            buf[i] = 0x20;
                            commacount = 0;
                        }
                        commacount++;
                    }
                    if(buf[i] == 0x22)
                    {
                        buf[i] = 0x20;
                    }
                }
            }
            sprintf(line2,"Location: %s",&buf[10]);
            c2 = 1;
            commacount = 0;
        }
        if ((strstr(buf,"temp_f")) && c3 == 0)
        {
            for(int i = 0; i<100; i++)
            {
                if(buf[i] == ',')
                {
                    buf[i] = ' ';
                    buf[i+1] = 'F';
                }
            }
            sprintf(line3,"Temperature: %s\n",&buf[11]);
            c3 = 1;
        }
        if ((strstr(buf,"relative_humidity")) && c4 == 0)
        {
           
            for(int i = 0; i < 100; i++)
            {
                if( (buf[i] == ',') || buf[i] == 0x22)
                {
                    buf[i] = 0x20;
                }
            }
            sprintf(line4,"Humidity: %s",&buf[23]);
            c4 = 1;
        }
        if ((strstr(buf,"wind_dir")) && c5 == 0)
        {
            for(int i = 0; i < 100; i++)
            {
                if( (buf[i] == ',') || buf[i] == 0x22 || buf[i] == 0x0A)
                {
                    if(buf[i] == 0x0A)
                    {
                        buf[i] = 0x00;
                    }
                    if((buf[i] == ',') || buf[i] == 0x22)
                    {
                        buf[i] = '\0';  
                    }
                    
                }

            }
            
           
            sprintf(line5,"Wind: %s ",&buf[14]);
            c5 = 1;
        }
        if ((strstr(buf,"wind_mph")) && c6 == 0)
        {
            for(int i = 0; i < 100; i++)
            {
                if( (buf[i] == ',') || buf[i] == 0x22 || buf[i] == 0x0A )
                {
                    if( (buf[i] == ',') || buf[i] == 0x22 )
                    {
                        buf[i] = 0x20;   
                    }
                    if(buf[i] == 0x0A )
                    {
                        buf[i] = 0x00; 
                        
                    }
                }
            }
            sprintf(line6,"%smph",&buf[13]);
            c6 = 1;
        }
        
    }
    printf("\n\n\n%s\n%s%s%s%s%s%s\n\n\n\n",line0,line1,line2,line3,line4,line5,line6);
    
    fclose(sock);
    close(sockfd);

}







